<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class n-maket extends Component
{
    /**
     * Create a new component instance.
     */

    function __construct($name, $url, $img)
    {
        public
        $name = $name;
        public
        $url = $url;
        public
        $img = $img;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public
    function render(): View|Closure|string
    {
        return view('components.n-maket');
    }
}
