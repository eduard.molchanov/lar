<form action="{{ route('data.store') }}" method="post">
    @csrf
    <div class="file-field input-field">
        <div class="btn">
            <span>File</span>
            <input type="file" name="data-file">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
        </div>
    </div>

</form>
