<div class="container fon">

    <div class="section z-depth-5">

        <ul class="collapsible popout " data-collapsible="accordion">
            <li>
                <div class="collapsible-header fon_c">
                    <i class="material-icons i">speaker_notes</i>
                    Объемные модели из бумаги
                </div>
                <div class="collapsible-body center-align fon_c p">
                    <p>
                        Пространственное воображение просто необходимо при изучении геометрии.
                        А у большинства современных подростков оно слабо развито и это очень тормозит
                        и затрудняет процесс познания. Но проблема решаема. И одним из способов решения
                        является создание объёмных моделей из бумаги. Милые зверушки, птички и рыбки,
                        сделанные Вашими руками, помогут Вам развить своё пространственное воображение.
                    </p>

                </div>
            </li>
            <?php
            include_once 's/m8.php';
            include_once 's/m2.php';
            include_once 's/m3.php';
            include_once 's/m4.php';
            include_once 's/m5.php';
            include_once 's/m6.php';
            include_once 's/m7.php';
            include_once 's/m1.php';
            ?>
<x-n-maket name="Белый медведь" img="img/1.jpg" url="https://drive.google.com/file/d/0ByXzgLdgskvZQW9oelN3UVptYU0/view?usp=sharing"/>
<x-n-maket name="Волк" img="img/2.jpg" url="https://drive.google.com/file/d/0ByXzgLdgskvZNUJYMVN6anhfQ1E/view?usp=sharing"/>
<x-n-maket name="Кабан" img="img/3.jpg" url="https://drive.google.com/file/d/0ByXzgLdgskvZRXA5bWgyTlhZelk/view?usp=sharing"/>
        </ul>
    </div>

</div>  <!--class="container"-->
